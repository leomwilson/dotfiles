# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=** r:|=** l:|=*'
zstyle :compinstall filename '/home/lwilson/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt extendedglob
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
### Added by Zplugin's installer
source '/home/lwilson/.zplugin/bin/zplugin.zsh'
autoload -Uz _zplugin
(( ${+_comps} )) && _comps[zplugin]=_zplugin
### End of Zplugin's installer chunk

# Zplugin
zplugin light iboyperson/pipenv-zsh
zplugin light iboyperson/zsh-gradle
zplugin load zsh-users/zsh-syntax-highlighting

zplugin load romkatv/powerlevel10k
zplugin ice from"gitlab"
zplugin light leomwilson/belleza
# source $HOME/git/lab/leomwilson/belleza/belleza.zsh

# Pista
#autoinit -Uz add-zsh-hook
#export SHORTEN_CWD=0
#export EXPAND_TILDE=0
#export CWD_COLOR="bright blue"
#export PROMPT_CHAR_COLOR="bright magenta"
#_pista_prompt() {
#    PROMPT=`/home/lwilson/git/hub/NerdyPepper/pista/target/release/pista -z`
#    # PROMPT=`pista`   # `pista -m` for the miminal variant
#}
#add-zsh-hook precmd _pista_prompt

# Aliases
alias ls="exa -a"
alias git="hub"
alias xmr="xmonad --recompile && xmonad --restart"
alias lunduke="telnet bbs.lunduke.com 23"
alias rrc="source ~/.zshrc"
# Env vars
export EDITOR=emacs
export PATH="$HOME/bin:$HOME/bin/adb-fastboot/platform-tools:$HOME/.cargo/bin:$PATH"
