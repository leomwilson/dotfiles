# Dotfiles

My Arch Linux dotfiles from my main computer.

I use Doom EMACS, and my personal configuration can be found [here](https://gitlab.com/leomwilson/doom-conf)

Everything goes in the home directory except:
- `pacman-hooks`

Credits:
- [iboyperson](https://github.com/iboyperson/dotfiles-public) for the foundation of my EMACS dotfiles and some of my pacman hooks
- [Derek Taylor](https://gitlab.com/dwt1/dotfiles) for a large section of my XMonad config
