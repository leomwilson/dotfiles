#!/bin/sh

# configure the display locations
xrandr --output HDMI-0 --primary --left-of DVI-D-0 &

# start compton
compton --config ~/.xmonad/compton.conf &

# set background
feh --bg-scale ~/.xmonad/bg/poly-planet.jpg &

# start PulseAudio if it isn't already
if start-pulseaudio-x11 ; then
    # PulseAudio is running, no need to start it
else
    pulseaudio --daemonize
fi
